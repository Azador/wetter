#
#
#

import Adafruit_BMP.BMP085 as BMP085

if __name__ == "__main__":
    sensor = BMP085.BMP085 (mode=BMP085.BMP085_ULTRAHIGHRES)
    t = sensor.read_temperature ()
    p = sensor.read_pressure () / 100.0
    print (repr ((p, t)))
