#
#
#

import os
import sys
import Adafruit_DHT

if __name__ == "__main__":
    if not os.path.exists ("/run/wetter"):
        os.mkdir ("/run/wetter")
        os.chown ("/run/wetter", 1228, 1000)
        
    ports_allowed = [4, 10, 17, 18, 22, 23, 24, 27]
    port = ports_allowed[0]
    if len (sys.argv) >= 2:
        port = int (sys.argv[1])
        if not port in ports_allowed:
            print ("(None, None )")
            exit (0)

    print (repr (Adafruit_DHT.read (Adafruit_DHT.DHT22, port)))
