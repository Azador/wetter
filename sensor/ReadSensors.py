#
#

import sys
import os
import datetime
import time
import subprocess
import shutil
import math
import numpy

actual_file="/run/wetter/wetter.dat"
history_dir="/home/harry/wetter"

def readDHT (port=4, retrys=5):
    i = 0
    while True:
        p = os.popen ('sudo /usr/bin/python3 /root/read_DHT.py {}'.format (port))
        for l in p:
            try:
                h,t = eval (l)

                if h != None and t != None:
                    return (h, t)
            except:
                pass
        
        if i >= retrys:
            return (None, None)

        i = i + 1
        time.sleep (2)

def readBMP180 ():
    p = os.popen ('sudo /usr/bin/python3 /root/read_BMP180.py')
    for l in p:
        try:
            p,t = eval (l)

            if p != None and t != None:
                return (p, t)
        except:
            pass
    return (None, None)

def appendActualData (ti, hum_inside, temp_inside, hum_outside, temp_outside, \
                      pressure):
    f = open (actual_file, 'at')
    f.write ("{} {} {} {} {} {} {:.1f} {:.1f} {:.1f} {:.1f} {:.2f}\n" \
             .format (ti.year, ti.month, ti.day, ti.hour, ti.minute, ti.second,
                      temp_inside, hum_inside, temp_outside, hum_outside,
                      pressure))
    f.close ()

def readLastActualEntry ():
    try:
        f = open (actual_file, 'rt')
        last_entry = None
        for l in f:
            #print ("read:", l)
            parts = l.split (' ')
            if len (parts) >= 6:
                last_entry = \
                    [datetime.datetime (int (parts[0]), int (parts[1]),
                                        int (parts[2]), int (parts[3]),
                                        int (parts[4]), int (parts[5]))]
                
                for i in range (6, len(parts)):
                    last_entry.append (float (parts[i]))
        #print ("last:", repr (last_entry))
        return last_entry
    except:
        pass
    return None

class Adjust:
    def __init__ (self, unknown, right_sides):
        self.unknown = unknown
        self.N = numpy.zeros ( (unknown, unknown) )
        self.r = numpy.zeros ( (unknown, right_sides) )

    def printMatrix (self):
        print ("N:")
        print (self.N)

    def addObservation (self, v, r):
        if len (v) != self.unknown:
            raise RangeError ("v has wrong size")

        v2 = v.reshape (1, self.unknown)
        self.N += v2.T * v2
        self.r += r * v

    def solve (self):
        res = numpy.linalg.solve (self.N, self.r).reshape (self.unknown)
        self.N = numpy.zeros ( (self.unknown, self.unknown) )
        self.r = numpy.zeros ( (self.unknown,) )
        return res
    
        for i in range (self.unknown):
            for j in range (i+1):
                sum = self.N[i][j]
                for k in range (j):
                    sum = sum - self.N[i][k] \
                                * self.N[j][k]
                if i > j:
                    self.N[i][j] = sum / self.N[j][j]
                elif sum > 0:
                    self.N[i][i] = math.sqrt (sum)
                else:
                    raise RangeError ("Not positiv definit")

        # Vorwärtseinsetzen
        y = self.unknown * [0.0]
        for i in range (self.unknown):
            sum = 0.0
            for k in range (i):
                sum += self.N[i][k] * y[k]
            y[i] = (self.r[i] - sum) / self.N[i][i]

        # Rückwärtseinsetzen
        x = self.unknown * [0.0]
        for j in range (self.unknown):
            i = self.unknown - j - 1
            sum = 0.0
            for k in range (i+1, self.unknown):
                sum += self.N[i][k] * x[k]
            x[i] = (y[i] - sum) / self.N[i][i]

        self.N = numpy.zeros ( (self.unknown, self.unknown) )
        self.r = numpy.zeros ( (self.unknown,) )

        return x

SOLUTION_CONTINUE = 0
SOLUTION_FINISHED = 1
SOLUTION_DIVERGENT = 2

class Solver:
    def __init__ (self, problem):
        self.adj = Adjust (problem.numberOfUnknown (),
                           problem.numberOfEquations ())
        self.problem = problem

    def solve (self):
        if True:
            A = []
            b = []
            for obs in self.problem.getObservation ():
                A.append (obs[0])
                b.append (obs[1])

            #print ("b:", repr (b))
            res = numpy.linalg.lstsq (A, b)
            #print ("r:", res[0].T)
            r = []
            for ra in res[0].T:
                r.append (Polynom (ra))
            return (True, r, 1, 0.0)

        else:
            iteration = 0
            while True:
                for obs in self.problem.getObservation ():
                    self.adj.addObservation (*obs)

                x = self.adj.solve ()
                r = self.problem.setAndTestSolution (x, iteration)

                if r == SOLUTION_FINISHED:
                    return (True, self.problem.getSolution (), iteration, x)
                elif r == SOLUTION_DIVERGENT:
                    return (False, self.problem.getSolution (), iteration, x)

                iteration += 1
            
            return (False, self.problem.getSolution (), iteration, x)

class Polynom:
    def __init__ (self, a):
        self.a = a

    def solve (self, x):
        s = 0.0
        for i in range (len (self.a)):
            s += self.a[i] * x ** i
        return s

    def __repr__ (self):
        return "Polynom (" + repr (self.a) + ")"
    
class ProblemPoly:
    """ Ausgleich eines Polynoms.
    Es werden die Koeffizienten eines Polynoms
    y = Summe (a[i] x ** i) für i: 0 <= i <= degree
    bestimmt für die Datentupel x, y = data[j]
    """
    max_iteration = 5
    
    def __init__ (self, data, degree, epsilon=1e-1):
        if len (data) < degree:
            raise ValueError ("data must have at least degree entries.")
        
        self.data = data
        self.degree = degree
        self.epsilon = epsilon
        self.a = (degree+1) * [ 0.0 ]

    def numberOfUnknown (self):
        return self.degree+1

    def numberOfEquations (self):
        return len (self.data) - 1

    def getObservation (self):
        for d in self.data:
            y = [numpy.zeros ( (self.degree+1,) ), []]
            s = 0.0
            for i in range (self.degree+1):
                y[0][i] = d[0]**i
                s += self.a[i] * d[0]**i

            for r in d[1][:5]:
                y[1].append (r - s)
            #print ("Obs:", repr (y))
            yield y

    def getSolution (self):
        return Polynom (self.a)

    def setAndTestSolution (self, x, iteration):
        sum = 0.0
        for i in range (self.degree+1):
            self.a[i] += x[i]
            sum += x[i]**2

        sum /= self.degree+1
        if sum < self.epsilon:
            return SOLUTION_FINISHED

        if iteration < self.max_iteration:
            return SOLUTION_CONTINUE
        
        return SOLUTION_DIVERGENT

def interpolateValue (values, pos):
    """ Interpolation über min. drei Wertepaare in values.
    In Values wird ein Vektor von Tupeln mit je zwei Werten erwartet (x,y)
    Es wird ein Polynom zweiten Grades interpoliert und der Wert des Polynoms
    an der Stelle pos zurückgegeben.
    """
    if len (values) < 3:
        raise RangeError ("Not enough values")

    problem = ProblemPoly (values, 2)
    solver = Solver (problem)
    ok, solution, iteration, x = solver.solve ()

    if not ok:
        raise ValueError ("Solution failes: " + repr ((solution, iteration, x)))

    v = [ x.solve (pos) for x in solution]
    #print ("solution for " + repr (pos) + ": " + repr (v))
    return v

class MeasureStats:
    def __init__ (self, values=5):
        self.v = values * [0.0]
        self.c = values * [0]

    def addValues (self, l):
        for i in range (len (l)):
            if i >= len (self.v):
                self.v.append (l[i])
                self.c.append (1)
            else:
                self.v[i] += l[i]
                self.c[i] += 1

    def count (self):
        cm = 0
        for c in self.c:
            if c > cm:
                cm = c
        return cm

    def getAvgs (self):
        for i in range (len (self.v)):
            if self.c[i] > 0:
                yield self.v[i] / self.c[i]
            else:
                yield self.v[i]
                
def printValues (d, v, postchar=','):
    avgs = v
    print ('[[{},{},{},{},{},{}],'.format (d.year, d.month, d.day,
                                            d.hour, d.minute, d.second) +
           ','.join (['{:.1f}'.format (a) for a in avgs]) + ']' + postchar)

class MovingInterpolation:
    def __init__ (self):
        self.actual_timestamp = None
        self.data = []
        self.max_data = 4
        self.timestamp_inc = datetime.timedelta (minutes=2)
        self.n_values = 1000

    def addValues (self, timestamp, values):
        if self.actual_timestamp == None:
            self.actual_timestamp = timestamp

        if self.n_values > len (values):
            self.n_values = len (values)
        self.data.append ((timestamp, values))

        while len (self.data) > self.max_data and \
              (self.data[-1][0] - self.data[1][0]) >= self.timestamp_inc:
            del self.data[0]

    def needNewValues (self):
        if len (self.data) < self.max_data:
            return True

        if self.actual_timestamp > self.data[int ((len (self.data)+1)/2)][0]:
            return True

        return (self.data[-1][0] - self.data[0][0]) < self.timestamp_inc

    def getActualTimestamp (self):
        return self.actual_timestamp

    def setActualTimestamp (self, timestamp):
        self.actual_timestamp = timestamp

    def getActualValues (self, timestamp):
        d_start = self.data[0][0]
        d_pos = (timestamp - d_start).total_seconds ()
        r = []
        v = []

        #for i in range (self.n_values):
        v2 = [ ((x[0] - d_start).total_seconds (), x[1]) \
               for x in self.data ]
        r = interpolateValue (v2, d_pos)

        #print (repr ((self.actual_timestamp, r)))
        return (timestamp, r)

    def setTimestampIncrease (self, inc):
        self.timestamp_inc = inc

    def getValues (self):
        while self.actual_timestamp <= self.data[-1][0]+self.timestamp_inc:
            ts = self.actual_timestamp
            self.actual_timestamp += self.timestamp_inc
            yield self.getActualValues (ts)

def getValuesFromFile (file):
    f = open (file, 'rt')

    for l in f:
        n = l.split (' ')
        if n[0] == 'a':
            continue
        dn = [ int (e) for e in n[0:6] ]
        d = datetime.datetime (*dn)

        vn = [ float (e) for e in n[6:] ]

        yield [d, vn]


def writeData (f, b, d, vn):
    vs = [str (n) for n in [d.year, d.month, d.day, d.hour, d.minute, d.second]]
    vs += ['{:.2f}'.format (n) for n in vn]
    f.write (b + "{}\n".format (' '.join (vs)))
    
def compressActualData2 (fn_src, fn_dst):
    fdst = open (fn_dst, "wt")

    agregation_week = datetime.timedelta (minutes=30)
    agregation_month = datetime.timedelta (minutes=120)
    agregation_year = datetime.timedelta (hours=24)
    
    v = []
    mi = MovingInterpolation ()
    pre = ''

    for astat in range (4):
        if astat == 0:
            mi.setTimestampIncrease (agregation_year)
            pre = 'a 0 '
        elif astat == 1:
            mi.setTimestampIncrease (agregation_month)
            pre = 'a 1 '
        elif astat == 2:
            mi.setTimestampIncrease (agregation_week)
            pre = 'a 2 '
        elif astat == 3:
            pre = ''

        actual_timestamp_set = False

        last_d = None
        for d, vn in getValuesFromFile (fn_src):
            if astat == 3:
                writeData (fdst, pre, d, vn)
            else:
                if not actual_timestamp_set:
                    actual_timestamp_set = True
                    ts = None
                    if astat == 0:
                        ts = datetime.datetime (d.year, d.month, d.day, 12)
                    elif astat == 1:
                        ts = datetime.datetime (d.year, d.month, d.day, 1)
                    elif astat == 2:
                        ts = datetime.datetime (d.year, d.month, d.day, 0, 15)

                    #print ("set actual time:", ts)
                    mi.data = []
                    mi.setActualTimestamp (ts)

                mi.addValues (d, vn)

                while not mi.needNewValues ():
                    for v in mi.getValues ():
                        writeData (fdst, pre, v[0], v[1])
                        if mi.needNewValues ():
                            break
                last_d = d

        if astat < 3 and last_d.day == mi.getActualTimestamp ().day:
            for v in mi.getValues ():
                if v[0].day != last_d.day:
                    break
                writeData (fdst, pre, v[0], v[1])
                last_d = v[0]

def compressActualData (ti):
    dest_file_name = "{:4}_{:02}_{:02}.dat".format (ti.year, ti.month, ti.day)
    dest_file = os.path.join (history_dir, dest_file_name)
    compressActualData2 (actual_file, dest_file)
    os.remove (actual_file)

if __name__ == "__main__":
    if len (sys.argv) > 2:
        src_fn = sys.argv[1]
        dst_fn = sys.argv[2]
        if not os.path.exists (src_fn):
            print ("Quelle {} existiert nicht".format (src_fn))
            exit (1)

        compressActualData2 (src_fn, dst_fn)
        #os.remove (src_fn)
        exit ()

    last_entry = readLastActualEntry ()
    h_in , t_in  = readDHT (4, retrys=5)
    #print ("innen:", h_in, t_in)
    h_out, t_out = readDHT (17, retrys=15)
    #print ("aussen:", h_out, t_out)
    p, t2_in = readBMP180 ()
    #print ("druck:", p, t2_in)
        
    if h_in != None and t_in != None:
        if last_entry != None:
            if h_out == None:
                h_out = last_entry[4]
            if t_out == None:
                t_out = last_entry[3]
            if p == None and len (last_entry) >= 6:
                p = last_entry[5]
        else:
            if h_out == None:
                h_out = 0.0
            if t_out == None:
                t_out = -280.0
            if p == None:
                p = 0.0
        
        #print ("Innen : Temperatur: {:.1f}°C  rel. Luftfeuchtigkeit: {:.0f}%".format (t_in,h_in))
        #print ("Aussen: Temperatur: {:.1f}°C  rel. Luftfeuchtigkeit: {:.0f}%".format (t_out,h_out))
        #print ("Luftdruck: {:.2f} hPa".format (p))
        ti = datetime.datetime.now ()
        if last_entry != None and ti.day != last_entry[0].day:
            #print ("different day:", repr (last_entry), repr (ti))
            compressActualData (last_entry[0])
        #else:
        #    print ("same day:", repr (last_entry), repr (ti))
                
        appendActualData (ti, h_in, t_in, h_out, t_out, p)
