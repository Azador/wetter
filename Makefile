
HTML_DIR=$(HOME)/html
JS_DIR=$(HTML_DIR)/js
CGIBIN_DIR=$(HOME)/cgi-bin
BIN_DIR=$(HOME)
ROOTBIN_DIR=/root

BASE_HTML_DEST=index.html
CRON_SCHEDULE=*/5 * * * *
PYTHON=/usr/bin/python3

CP=cp
SUDO=sudo
CRONTAB=crontab
SED=sed

install: install-html install-js install-cgibin install-bin install-cron

install-html:
	$(CP) html/index.html $(HTML_DIR)/$(BASE_HTML_DEST)
	$(CP) html/stylesheet.css $(HTML_DIR)/

install-js:
	$(CP) html/js/Chart.bundle.min.js $(JS_DIR)/
	$(CP) html/js/plotData.js $(JS_DIR)/

install-cgibin:
	$(CP) cgi-bin/getData.py $(CGIBIN_DIR)/

install-bin:
	$(CP) sensor/ReadSensors.py $(BIN_DIR)/
	$(SUDO) $(CP) sensor/read_DHT.py $(ROOTBIN_DIR)/
	$(SUDO) $(CP) sensor/read_BMP180.py $(ROOTBIN_DIR)/

install-cron: #install-bin
	$(CRONTAB) -l | $(SED) -e "s/^.*$(subst /,\/,$(BIN_DIR))\/ReadSensors.py$$/$(subst /,\/,$(CRON_SCHEDULE)) $(subst /,\/,$(PYTHON)) $(subst /,\/,$(BIN_DIR))\/ReadSensors.py/" | $(CRONTAB) -

