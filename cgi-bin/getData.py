#!/usr/bin/python3
#

import os
import datetime
import math
import numpy

d_now = datetime.datetime.now ()
d_day   = d_now - datetime.timedelta (0, hours=24)
d_week  = d_now - datetime.timedelta (7)
d_month = d_now - datetime.timedelta (30)

agregation_day = datetime.timedelta (minutes=5)
agregation_week = datetime.timedelta (minutes=30)
agregation_month = datetime.timedelta (minutes=120)
agregation_year = datetime.timedelta (hours=24)

def getArchives ():
    archiv_dir = "/home/harry/wetter"

    archiv1 = [ (os.path.join (archiv_dir, fn), fn) \
                for fn in os.listdir (archiv_dir) ]

    archiv1.sort ()
    archiv = [ e for e in filter (lambda e: e[1].endswith ('.dat') or e[1].endswith ('.da2'), archiv1) ]

    existing_dat = {}
    remove_idx = []
    for i in range (len (archiv)):
        if archiv[i][1].endswith ('.da2'):
            for ed in existing_dat:
                if existing_dat[ed][0][1][:-2] == archiv[i][1][:-2]:
                    remove_idx.append (existing_dat[ed][1])
                    break
        else:
            exists = False
            for ed in existing_dat:
                if existing_dat[ed][0][1][:-2] == archiv[i][1][:-2]:
                    remove_idx.append (i)
                    exists = True
                    break
            if exists:
                continue
        existing_dat[archiv[i][1]] = (archiv[i], i)

    for i in remove_idx[-1::-1]:
        del archiv[i]


    start=0
    for fn in archiv:
        #print (fn)
        n = fn[1].split ("_")
        if len (n) >= 3:
            n[2] = n[2].split ('.')[0]

        try:
            d = datetime.datetime (int (n[0]), int (n[1]), int (n[2]), 23, 59, 59)
            #print (d, d_now - d)
            if d_now - d <= datetime.timedelta (366):
                break
        except Exception as e:
            print ("fn:", repr (fn), ":", repr (e))

        start = start+1

    archiv.append (("/run/wetter/wetter.dat", "wetter.dat"))
    return (start, archiv)

class Adjust:
    def __init__ (self, unknown, right_sides):
        self.unknown = unknown
        self.N = numpy.zeros ( (unknown, unknown) )
        self.r = numpy.zeros ( (unknown, right_sides) )

    def printMatrix (self):
        print ("N:")
        print (self.N)

    def addObservation (self, v, r):
        if len (v) != self.unknown:
            raise RangeError ("v has wrong size")

        v2 = v.reshape (1, self.unknown)
        self.N += v2.T * v2
        self.r += r * v

    def solve (self):
        res = numpy.linalg.solve (self.N, self.r).reshape (self.unknown)
        self.N = numpy.zeros ( (self.unknown, self.unknown) )
        self.r = numpy.zeros ( (self.unknown,) )
        return res
    
        for i in range (self.unknown):
            for j in range (i+1):
                sum = self.N[i][j]
                for k in range (j):
                    sum = sum - self.N[i][k] \
                                * self.N[j][k]
                if i > j:
                    self.N[i][j] = sum / self.N[j][j]
                elif sum > 0:
                    self.N[i][i] = math.sqrt (sum)
                else:
                    raise RangeError ("Not positiv definit")

        # Vorwärtseinsetzen
        y = self.unknown * [0.0]
        for i in range (self.unknown):
            sum = 0.0
            for k in range (i):
                sum += self.N[i][k] * y[k]
            y[i] = (self.r[i] - sum) / self.N[i][i]

        # Rückwärtseinsetzen
        x = self.unknown * [0.0]
        for j in range (self.unknown):
            i = self.unknown - j - 1
            sum = 0.0
            for k in range (i+1, self.unknown):
                sum += self.N[i][k] * x[k]
            x[i] = (y[i] - sum) / self.N[i][i]

        self.N = numpy.zeros ( (self.unknown, self.unknown) )
        self.r = numpy.zeros ( (self.unknown,) )

        return x

SOLUTION_CONTINUE = 0
SOLUTION_FINISHED = 1
SOLUTION_DIVERGENT = 2

class Solver:
    def __init__ (self, problem):
        self.adj = Adjust (problem.numberOfUnknown (),
                           problem.numberOfEquations ())
        self.problem = problem

    def solve (self):
        if True:
            A = []
            b = []
            for obs in self.problem.getObservation ():
                A.append (obs[0])
                b.append (obs[1])

            #print ("b:", repr (b))
            res = numpy.linalg.lstsq (A, b)
            #print ("r:", res[0].T)
            r = []
            for ra in res[0].T:
                r.append (Polynom (ra))
            return (True, r, 1, 0.0)

        else:
            iteration = 0
            while True:
                for obs in self.problem.getObservation ():
                    self.adj.addObservation (*obs)

                x = self.adj.solve ()
                r = self.problem.setAndTestSolution (x, iteration)

                if r == SOLUTION_FINISHED:
                    return (True, self.problem.getSolution (), iteration, x)
                elif r == SOLUTION_DIVERGENT:
                    return (False, self.problem.getSolution (), iteration, x)

                iteration += 1
            
            return (False, self.problem.getSolution (), iteration, x)

class Polynom:
    def __init__ (self, a):
        self.a = a

    def solve (self, x):
        s = 0.0
        for i in range (len (self.a)):
            s += self.a[i] * x ** i
        return s

    def __repr__ (self):
        return "Polynom (" + repr (self.a) + ")"
    
class ProblemPoly:
    """ Ausgleich eines Polynoms.
    Es werden die Koeffizienten eines Polynoms
    y = Summe (a[i] x ** i) für i: 0 <= i <= degree
    bestimmt für die Datentupel x, y = data[j]
    """
    max_iteration = 5
    
    def __init__ (self, data, degree, epsilon=1e-1):
        if len (data) < degree:
            raise ValueError ("data must have at least degree entries.")
        
        self.data = data
        self.degree = degree
        self.epsilon = epsilon
        self.a = (degree+1) * [ 0.0 ]

    def numberOfUnknown (self):
        return self.degree+1

    def numberOfEquations (self):
        return len (self.data) - 1

    def getObservation (self):
        for d in self.data:
            y = [numpy.zeros ( (self.degree+1,) ), []]
            s = 0.0
            for i in range (self.degree+1):
                y[0][i] = d[0]**i
                s += self.a[i] * d[0]**i

            for r in d[1][:5]:
                y[1].append (r - s)
            #print ("Obs:", repr (y))
            yield y

    def getSolution (self):
        return Polynom (self.a)

    def setAndTestSolution (self, x, iteration):
        sum = 0.0
        for i in range (self.degree+1):
            self.a[i] += x[i]
            sum += x[i]**2

        sum /= self.degree+1
        if sum < self.epsilon:
            return SOLUTION_FINISHED

        if iteration < self.max_iteration:
            return SOLUTION_CONTINUE
        
        return SOLUTION_DIVERGENT

def interpolateValue (values, pos):
    """ Interpolation über min. drei Wertepaare in values.
    In Values wird ein Vektor von Tupeln mit je zwei Werten erwartet (x,y)
    Es wird ein Polynom zweiten Grades interpoliert und der Wert des Polynoms
    an der Stelle pos zurückgegeben.
    """
    if len (values) < 3:
        raise RangeError ("Not enough values")

    problem = ProblemPoly (values, 2)
    solver = Solver (problem)
    ok, solution, iteration, x = solver.solve ()

    if not ok:
        raise ValueError ("Solution failes: " + repr ((solution, iteration, x)))

    v = [ x.solve (pos) for x in solution]
    #print ("solution for " + repr (pos) + ": " + repr (v))
    return v

class MeasureStats:
    def __init__ (self, values=5):
        self.v = values * [0.0]
        self.c = values * [0]

    def addValues (self, l):
        for i in range (len (l)):
            if i >= len (self.v):
                self.v.append (l[i])
                self.c.append (1)
            else:
                self.v[i] += l[i]
                self.c[i] += 1

    def count (self):
        cm = 0
        for c in self.c:
            if c > cm:
                cm = c
        return cm

    def getAvgs (self):
        for i in range (len (self.v)):
            if self.c[i] > 0:
                yield self.v[i] / self.c[i]
            else:
                yield self.v[i]
                
def printValues (d, v, postchar=','):
    avgs = v
    print ('[[{},{},{},{},{},{}],'.format (d.year, d.month, d.day,
                                            d.hour, d.minute, d.second) +
           ','.join (['{:.1f}'.format (a) for a in avgs]) + ']' + postchar)

class MovingInterpolation:
    def __init__ (self):
        self.actual_timestamp = None
        self.data = []
        self.max_data = 4
        self.timestamp_inc = datetime.timedelta (minutes=2)
        self.n_values = 1000

    def addValues (self, timestamp, values):
        if self.actual_timestamp == None:
            self.actual_timestamp = timestamp

        if self.n_values > len (values):
            self.n_values = len (values)
        self.data.append ((timestamp, values))

        while len (self.data) > self.max_data and \
              (self.data[-1][0] - self.data[1][0]) >= self.timestamp_inc:
            del self.data[0]

    def needNewValues (self):
        if len (self.data) < self.max_data:
            return True

        if self.actual_timestamp > self.data[int ((len (self.data)+1)/2)][0]:
            return True

        return (self.data[-1][0] - self.data[0][0]) < self.timestamp_inc

    def getActualTimestamp (self):
        return self.actual_timestamp

    def setActualTimestamp (self, timestamp):
        self.actual_timestamp = timestamp

    def getActualValues (self, timestamp):
        d_start = self.data[0][0]
        d_pos = (timestamp - d_start).total_seconds ()
        r = []
        v = []

        #for i in range (self.n_values):
        v2 = [ ((x[0] - d_start).total_seconds (), x[1]) \
               for x in self.data ]
        r = interpolateValue (v2, d_pos)

        #print (repr ((self.actual_timestamp, r)))
        return (timestamp, r)

    def setTimestampIncrease (self, inc):
        self.timestamp_inc = inc

    def getTimestampIncrease (self):
        return self.timestamp_inc

    def getValues (self):
        while self.actual_timestamp <= self.data[-1][0]+self.timestamp_inc:
            ts = self.actual_timestamp
            self.actual_timestamp += self.timestamp_inc
            yield self.getActualValues (ts)

skip_rest_of_this_file_after_type = None

def getValuesFromFiles (files):
    global skip_rest_of_this_file_after_type
    for i in range (len (files)):
        #print ("open:", files[i][1])
        f = open (files[i][0], 'rt')
        for l in f:
            n = l.split (' ')
            t = 0
            if n[0] == 'a':
                t = 3-int (n[1])
                n = n[2:]

            if skip_rest_of_this_file_after_type != None \
               and t != skip_rest_of_this_file_after_type:
                #print ("skip after:", skip_rest_of_this_file_after_type)
                #print ("  file:", i, files[i][1])
                skip_rest_of_this_file_after_type = None
                break
                
            dn = [ int (e) for e in n[0:6] ]
            d = datetime.datetime (*dn)
            vn = [ float (e) for e in n[6:] ]
            yield [t, d, vn]

def getAllValuesFromFiles (archive_files):
    global skip_rest_of_this_file_after_type
    #sums = MeasureStats ()
    #old_date = datetime.datetime (2000, 1, 1)
    #block_size = agregation_year

    v = []
    mi = MovingInterpolation ()
    mi.setTimestampIncrease (agregation_year)
    astat = 4
    
    for t, d, vn in getValuesFromFiles (archive_files):
        if astat == 4:
            ts = d
            if d < d_month:
                ts = datetime.datetime (d.year, d.month, d.day)
            elif d < d_week:
                ts = datetime.datetime (d.year, d.month, d.day,
                                        round (d.hour/2) * 2)

            elif d < d_day:
                ts = datetime.datetime (d.year, d.month, d.day, d.hour,
                                        round (d.minute/30)*30)

            else:
                ts = datetime.datetime (d.year, d.month, d.day, d.hour,
                                        round (d.minute/5)*5)
            
            mi.setActualTimestamp (ts)
            astat = 3
            
        if d >= d_day and astat > 0 and t == astat:
            mi.setTimestampIncrease (agregation_day)
            astat = 0
            skip_rest_of_this_file_after_type = None
            #print ("Increase 5 min")
            #print (d, ">=", d_day)
        elif d >= d_week and astat > 1 and t == astat:
            mi.setTimestampIncrease (agregation_week)
            astat = 1
            skip_rest_of_this_file_after_type = None
            #print ("Increase 30 min")
        elif d >= d_month and astat > 2 and t == astat:
            mi.setTimestampIncrease (agregation_month)
            astat = 2
            skip_rest_of_this_file_after_type = None
            #print ("Increase 2 h")

        #print ("v:", repr ((t,d,vn)))
        if astat == t:
            if skip_rest_of_this_file_after_type != t:
                skip_rest_of_this_file_after_type = t
                #print ("skip after:", t)
        elif t != 0:
            continue
        
        #print ("f:", d, vn, "data:", len (mi.data), mi.timestamp_inc)
        if t != 0:
            mi.setActualTimestamp (d+mi.getTimestampIncrease ())
            yield (d, vn)
        else:
            mi.addValues (d, vn)

            while not mi.needNewValues ():
                for v in mi.getValues ():
                    #print ("data:", repr (mi.data))
                    yield (v[0], v[1])
                    if mi.needNewValues ():
                        break

if __name__ == "__main__":
    try:
        start, archiv = getArchives ()

        print ("Content-type: text/json\n");
        print ('[')

        lv0, lv1 = (None, None)
        for v0, v1 in getAllValuesFromFiles (archiv[start:]):
            if lv0 != None:
                printValues (lv0, lv1)
            lv0 = v0
            lv1 = v1
        
        printValues (lv0, lv1, "]")
    except Exception as e:
        if True:
            print ("Exception:", repr (e))
        else:
            raise e
