"use strict";
//
//
//

var request = new XMLHttpRequest();

function wasserPartialdruck (t, h_rel) {
    var tk = t + 273.15;
    // Sättigungsdampfdruck berechnen, Quelle:
    // https://de.wikibooks.org/wiki/Tabellensammlung_Chemie/_Stoffdaten_Wasser#S%C3%A4ttigungsdampfdruck
    // Korrekturfaktor ca. 1.005 für Luft mit Normaldruck
    var satt = Math.exp (-6094.4642/tk + 21.1249952
			 - 2.7245552e-2 * tk + 1.6853396e-5 * tk * tk
			 + 2.4575506 * Math.log (tk)) * 1.005 / 100.0;
    var part = satt * h_rel / 100.0;
    return Math.round (part*10) / 10.0;
}

function updateData () {
    request.open ("GET","cgi-bin/getData.py");
    //request.onreadystatechange = handleContent;
    request.addEventListener ('load', contentLoaded);
    request.send ();
}

updateData ();

window.setInterval (updateData, 120000);

function contentLoaded (event) {
    //request.responseText;
    //alert ("Loaded: " + request.status);

    if (request.status >= 200 && request.status < 300) {
	var data = JSON.parse (request.responseText);
	data.forEach ((d) => {
	    if (d.length <= 6) {
		d.push (wasserPartialdruck (d[1], d[2]));
	    } else {
		d[6] = wasserPartialdruck (d[1], d[2]);
	    }

	    if (d.length <= 7) {
		d.push (wasserPartialdruck (d[3], d[4]));
	    } else {
		d[7] = wasserPartialdruck (d[3], d[4]);
	    }
	});
	insertDiagramData (data);
    }
}

function clearChart (chart) {
    chart.data.labels = [];
    chart.data.datasets.forEach((dataset) => {
        dataset.data = [];
    });
}

function f (i) {
    return (""+i).padStart (2, "0");
}

function timeToString (t) {
    return `${t[2]}.${t[1]}.${t[0]} ${f(t[3])}:${f(t[4])}`;
}

function pushLabels (d) {
    var s = timeToString (d[0]);

    window.chart_humtemp_innen.data.labels.push (s);
    window.chart_humtemp_aussen.data.labels.push (s);
    window.chart_pressure.data.labels.push (s);
    window.chart_partial_pressure.data.labels.push (s);
}

function setElementContentById (id, value) {
    var d = document.getElementById (id);
    if (d.childNodes.length == 0) {
	d.appendChild (document.createTextNode (value));
    } else {
	d.replaceChild (document.createTextNode (value), d.firstChild);
    }
}

function insertDiagramData (data) {
    //alert (data.length);
    
    clearChart (window.chart_humtemp_innen);
    clearChart (window.chart_humtemp_aussen);
    clearChart (window.chart_pressure);
    clearChart (window.chart_partial_pressure);

    var last_day=-1;
    
    data.forEach ((d) => {
	pushLabels (d);
	
	window.chart_humtemp_innen.data.datasets[0].data.push (d[1]);
	window.chart_humtemp_innen.data.datasets[1].data.push (d[2]);
	window.chart_humtemp_aussen.data.datasets[0].data.push (d[3]);
	window.chart_humtemp_aussen.data.datasets[1].data.push (d[4]);
	window.chart_pressure.data.datasets[0].data.push (d[5]);
	window.chart_partial_pressure.data.datasets[0].data.push (d[6]);
	window.chart_partial_pressure.data.datasets[1].data.push (d[7]);
    });
    
    window.chart_humtemp_innen.update ();
    window.chart_humtemp_aussen.update ();
    window.chart_pressure.update ();
    window.chart_partial_pressure.update ();

    var i = data.length - 1;
    setElementContentById ('Temperatur_innen', data[i][1].toFixed (1));
    setElementContentById ('Luftfeuchtigkeit_innen', data[i][2].toFixed (1));
    setElementContentById ('Temperatur_aussen', data[i][3].toFixed (1));
    setElementContentById ('Luftfeuchtigkeit_aussen', data[i][4].toFixed (1));
    setElementContentById ('Luftdruck', data[i][5].toFixed (1));
    setElementContentById ('PartialdruckH2O_innen', data[i][6].toFixed (1));
    setElementContentById ('PartialdruckH2O_aussen', data[i][7].toFixed (1));
    setElementContentById ('Timestamp', timeToString (data[i][0]));
}
      
var red    = 'rgba(180, 80, 80, 1)';
var blue   = 'rgba(80, 80, 180, 1)';
var green  = 'rgba(70, 170, 70, 1)';
var orange = 'rgba(240, 180, 0, 1)';
var temp_hum_scales =
    {
        xAxes: [{
	    display: true,
	    scaleLabel: {
                display: false,
                labelString: ''
	    }
        }],
        yAxes: [{
	    display: true,
	    id: 'temp',
	    ticks: {
		min: -10,
		max: 40,
	    },
	    scaleLabel: {
                display: true,
                labelString: 'Temperatur [C]'
	    }
        },{
	    display: true,
	    position: 'right',
	    id: 'hum',
	    ticks: {
		min: 0,
		max: 100,
	    },
	    scaleLabel: {
                display: true,
                labelString: 'rel. Luftfeuchtigkeit [%]'
	    },
	    gridLines: {
		display: false
	    }
        }]
    };

var pressure_scales =
    {
        xAxes: [{
	    display: true,
	    scaleLabel: {
                display: false,
                labelString: ''
	    }
        }],
        yAxes: [{
	    display: true,
	    id: 'pressure',
	    ticks: {
		min: 950,
		max: 1050,
	    },
	    scaleLabel: {
                display: true,
                labelString: 'Luftdruck [hPa]'
	    }
        }]
    };

var partial_pressure_scales =
    {
        xAxes: [{
	    display: true,
	    scaleLabel: {
                display: false,
                labelString: ''
	    }
        }],
        yAxes: [{
	    display: true,
	    id: 'pressure',
	    ticks: {
		min: 0.0,
		max: 40.0,
	    },
	    scaleLabel: {
                display: true,
                labelString: 'Partialdruck H2O [hPa]'
	    }
        }]
    };

function getOptions (where, scales) {
    return {
	animation: { duration: 0 },
        responsive: true,
        title: {
            display: true,
            text: where
        },
        tooltips: {
            mode: 'index',
            intersect: false,
        },
        hover: {
            mode: 'nearest',
            intersect: true
        },
        scales: scales
    }
};

function dataSetConfig (label, color, y_axis_id) {
    return { 
        label: label,
	yAxisID: y_axis_id,
        backgroundColor: color,
        borderColor: color,
	borderWidth: 1,
	pointRadius: 0,
	cubicInterpolationMode: 'monotone',
        data: [],
        fill: false,
    };
}

var config_innen =
    { type: 'line',
      data: {
          labels: [],
          datasets: [dataSetConfig ('Temp. innen', red, 'temp'),
		     dataSetConfig ('rel. F. innen', blue, 'hum')]
      },
      options: getOptions ('Innen', temp_hum_scales)
    };

var config_aussen =
    { type: 'line',
      data: {
          labels: [],
          datasets: [dataSetConfig ('Temp. außen', red, 'temp'),
		     dataSetConfig ('rel. F. außen', blue, 'hum')]
      },
      options: getOptions ('Außen', temp_hum_scales)
    };
      
var config_pressure =
    { type: 'line',
      data: {
          labels: [],
          datasets: [dataSetConfig ('Luftdruck', orange, 'pressure')]
      },
      options: getOptions ('Luftdruck', pressure_scales)
    };
      
var config_partial_pressure =
    { type: 'line',
      data: {
          labels: [],
          datasets: [dataSetConfig ('Part. Druck. innen', blue, 'pressure'),
		     dataSetConfig ('Part. Druck. außen', green, 'pressure')]
      },
      options: getOptions ('Partialdruck H2O', partial_pressure_scales)
    };
      
window.onload = function() {
    var ctx = document.getElementById ('temp_hum_innen').getContext ('2d');
    window.chart_humtemp_innen  = new Chart (ctx, config_innen);
    ctx = document.getElementById ('temp_hum_aussen').getContext ('2d');
    window.chart_humtemp_aussen = new Chart (ctx, config_aussen);
    ctx = document.getElementById ('pressure').getContext ('2d');
    window.chart_pressure = new Chart (ctx, config_pressure);
    ctx = document.getElementById ('partial_pressure').getContext ('2d');
    window.chart_partial_pressure = new Chart (ctx, config_partial_pressure);
};

